package utils

// CustomError - Self defined
type CustomError struct {
	Code    uint8
	Message string
}

func (e *CustomError) Error() string {
	return e.Message
}
