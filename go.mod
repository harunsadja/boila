module github.com/harunrasyid/boila

go 1.14

require (
	github.com/armon/consul-api v0.0.0-20180202201655-eb2c6b5be1b6 // indirect
	github.com/cosmtrek/air v1.12.1 // indirect
	github.com/fatih/color v1.9.0 // indirect
	github.com/gorilla/mux v1.7.4
	github.com/howeyc/fsnotify v0.9.0 // indirect
	github.com/imdario/mergo v0.3.9 // indirect
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/pelletier/go-toml v1.8.0 // indirect
	github.com/pilu/config v0.0.0-20131214182432-3eb99e6c0b9a // indirect
	github.com/pilu/fresh v0.0.0-20190826141211-0fa698148017 // indirect
	github.com/sarulabs/di v2.0.0+incompatible
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/viper v1.7.0
	github.com/ugorji/go v1.1.7 // indirect
	github.com/xordataexchange/crypt v0.0.3-0.20170626215501-b2862e3d0a77 // indirect
	go.mongodb.org/mongo-driver v1.3.4
	golang.org/x/sys v0.0.0-20200615200032-f1bc736245b1 // indirect
)
