package routes

import (
	"github.com/gorilla/mux"
	"github.com/harunrasyid/boila/app/handlers"
	"github.com/harunrasyid/boila/app/repositories"
	"github.com/sarulabs/di"
)

// RouteUser - List of user route
func RouteUser(router *mux.Router, di di.Container) {
	handler := handlers.User{
		Repository: di.Get("repository.user").(*repositories.User),
	}
	user := router.PathPrefix("/user").Subrouter()
	user.HandleFunc("/{id}", handler.GetByID).Methods("GET")
}
