package routes

import (
	"github.com/gorilla/mux"
	"github.com/harunrasyid/boila/app/middlewares"
	"github.com/sarulabs/di"
	"github.com/spf13/viper"
)

// Initialize route
func Initialize(di di.Container, config *viper.Viper) *mux.Router {
	router := mux.NewRouter()

	RouteUser(router, di)

	context := middlewares.ContextMiddleware{Config: config}
	router.Use(context.Middleware)
	return router
}
