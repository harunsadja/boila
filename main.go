package main

import (
	"log"
	"net/http"
	"runtime"

	"github.com/harunrasyid/boila/di"
	"github.com/harunrasyid/boila/routes"
	"github.com/spf13/viper"
)

func main() {
	// Load config
	log.Println("Getting Config...")
	config := viper.New()

	config.AddConfigPath(".")
	config.SetConfigName(".env")
	config.AutomaticEnv()
	config.ReadInConfig()

	// Set goroutine max procs
	runtime.GOMAXPROCS(config.GetInt("app_max_procs"))

	// Initializing app container
	log.Println("Initializing app container...")
	app := di.InitContainer(config)
	staticDir := "/" + config.GetString("app_static") + "/"
	port := config.GetString("app_port")
	log.Println("App container loaded")

	// Initializing routes
	log.Println("Initializing routes...")
	r := routes.Initialize(app, config)
	r.PathPrefix(staticDir).Handler(
		http.StripPrefix(
			staticDir,
			http.FileServer(http.Dir("."+staticDir)),
		),
	)
	log.Println("Routes loaded")
	log.Printf("App Running on %s", port)
	http.ListenAndServe(":"+port, r)
}
