package di

import (
	"github.com/harunrasyid/boila/app/gateways"
	"github.com/harunrasyid/boila/app/repositories"
	"github.com/harunrasyid/boila/app/schemas"
	"github.com/sarulabs/di"
	"github.com/spf13/viper"
)

func addRepositories(builder *di.Builder, config *viper.Viper) {
	builder.Add([]di.Def{
		{
			Name: "repository.user",
			Build: func(ctn di.Container) (interface{}, error) {
				return &repositories.User{
					Gateway:     ctn.Get("gateway.user").(*gateways.User),
					Transformer: &schemas.UserTransformer{},
				}, nil
			},
		},
	}...)
}
