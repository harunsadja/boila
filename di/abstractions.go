package di

import (
	"github.com/harunrasyid/boila/app/abstractions"
	"github.com/sarulabs/di"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/mongo"
)

func addAbstractions(builder *di.Builder, config *viper.Viper) {
	builder.Add([]di.Def{
		{
			Name: "abstraction.mongo-db",
			Build: func(ctn di.Container) (interface{}, error) {
				return &abstractions.MongoDB{
					DB: ctn.Get("provider.mongo-db").(*mongo.Database),
				}, nil
			},
		},
	}...)
}
