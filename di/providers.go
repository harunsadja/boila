package di

import (
	"context"
	"fmt"
	"time"

	"github.com/sarulabs/di"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

func generateDSN(config *viper.Viper) string {
	return fmt.Sprintf(
		"mongodb://%s:%s@%s:%s/%s",
		config.GetString("db_username"),
		config.GetString("db_password"),
		config.GetString("db_host"),
		config.GetString("db_port"),
		config.GetString("db_name"),
	)
}

func addProviders(builder *di.Builder, config *viper.Viper) {
	builder.Add([]di.Def{
		{
			Name:  "provider.mongo-db",
			Scope: di.App,
			Build: func(ctn di.Container) (interface{}, error) {
				client, error := mongo.NewClient(
					options.Client().ApplyURI(generateDSN(config)),
				)

				ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
				error = client.Connect(ctx)
				if error != nil {
					return nil, error
				}

				ctx, _ = context.WithTimeout(context.Background(), 2*time.Second)
				error = client.Ping(ctx, readpref.Primary())
				if error != nil {
					return nil, error
				}

				var db *mongo.Database
				if error == nil {
					db = client.Database(config.GetString("db_name"))
				}
				return db, error
			},
		},
	}...)
}
