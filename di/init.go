package di

import (
	"github.com/sarulabs/di"
	"github.com/spf13/viper"
)

// InitContainer - Initialize all container
func InitContainer(config *viper.Viper) di.Container {
	builder, _ := di.NewBuilder()
	addProviders(builder, config)
	addAbstractions(builder, config)
	addGateways(builder, config)
	addRepositories(builder, config)

	return builder.Build()
}
