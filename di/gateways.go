package di

import (
	"github.com/harunrasyid/boila/app/abstractions"
	"github.com/harunrasyid/boila/app/gateways"
	"github.com/sarulabs/di"
	"github.com/spf13/viper"
)

func addGateways(builder *di.Builder, config *viper.Viper) {
	builder.Add([]di.Def{
		{
			Name: "gateway.user",
			Build: func(ctn di.Container) (interface{}, error) {
				return &gateways.User{
					DB: ctn.Get("abstraction.mongo-db").(abstractions.DBInterface),
				}, nil
			},
		},
	}...)
}
