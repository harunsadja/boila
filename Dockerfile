FROM golang:1.14.1

ENV GO111MODULE=on

COPY . /go/src/app
WORKDIR /go/src/app

RUN go get ./
RUN go build

ENV APP_NAME "Boila Test"
ENV APP_SCHEMA "http"
ENV APP_HOST "localhost"
ENV APP_PORT "8080"
ENV APP_ENV "dev"

ENV DB_HOST "mongodb"
ENV DB_PORT "27018"
ENV DB_USERNAME "boila"
ENV DB_PASSWORD "boila" 
ENV DB_NAME "boila_db"

CMD if [ ${APP_ENV} = prod ]; \
	then \
	app; \
	else \
	go get github.com/pilu/fresh && \
	fresh -c runner.conf; \
	fi
	
EXPOSE 8080