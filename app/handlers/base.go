package handlers

import (
	"encoding/json"
	"net/http"
	"net/url"
	"reflect"
	"strconv"
	"strings"

	"github.com/harunrasyid/boila/app/schemas"
)

func getSorting(q url.Values) map[string]string {
	filters := make(map[string]string)
	if q.Get("sort_by") != "" {
		filters["sort_by"] = q.Get("sort_by")
	} else {
		filters["sort_by"] = "id"
	}

	if q.Get("order") != "" {
		filters["order"] = strings.ToUpper(q.Get("order"))
	} else {
		filters["order"] = "ASC"
	}

	return filters
}

func getPage(q url.Values) int {
	var page string
	if q.Get("page") != "" {
		page = q.Get("page")
	} else {
		page = "1"
	}

	result, _ := strconv.Atoi(page)
	return result
}

// HTTPResult - Service result model
type HTTPResult struct {
	Data interface{}   `json:"data,omitempty"`
	Meta *schemas.Meta `json:"meta"`
}

// ResponseJSON - Self defined
func ResponseJSON(w http.ResponseWriter, data interface{}, meta *schemas.Meta) {
	status := meta.Status
	if status == 0 {
		status = http.StatusOK
	}

	result := HTTPResult{
		Meta: meta,
	}

	if data != nil && !reflect.ValueOf(data).IsNil() {
		result.Data = data
	}

	response, err := json.Marshal(result)
	if err != nil {
		status = http.StatusInternalServerError
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write(response)
	return
}
