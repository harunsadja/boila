package handlers

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/harunrasyid/boila/app/repositories"
)

// User handler struct
type User struct {
	Repository *repositories.User
}

// GetByID - Get single user by it's id
func (u *User) GetByID(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	vars := mux.Vars(r)
	id := vars["id"]

	data, meta := u.Repository.FindByID(ctx, id)
	ResponseJSON(w, data, meta)
}
