package models

import (
	"testing"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

func assertModel(t *testing.T, model Model) {
	model.Init()
	if model.GetID() == primitive.NilObjectID {
		t.Errorf("Failed to generate new object id")
	}

	if model.GetCreatedAt() == (time.Time{}) {
		t.Errorf("Failed to initiate created at")
	}

	model.Update()
	if model.GetUpdatedAt() == (time.Time{}) {
		t.Errorf("Failed to initiate updated at")
	}

	model.Delete()
	if model.GetDeletedAt() == (time.Time{}) {
		t.Errorf("Failed to initiate delete at")
	}
}
