package models

// User - data model of user
type User struct {
	BaseModel    `bson:",inline"`
	Username     string `json:"username"`
	Password     string `json:"password"`
	Fullname     string `json:"fullname"`
	Email        string `json:"email"`
	MobileNumber string `json:"mobile_number"`
	Enabled      bool   `json:"enabled"`
}
