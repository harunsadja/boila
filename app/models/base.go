package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Model - Interface of model
type Model interface {
	Init()
	Update()
	Delete()

	GetID() primitive.ObjectID
	GetCreatedAt() time.Time
	GetUpdatedAt() time.Time
	GetDeletedAt() time.Time
}

// IDField - ID field.
type IDField struct {
	ID primitive.ObjectID `bson:"_id,omitempty"`
}

// Init - Create action for id field
func (m *IDField) Init() {
	m.ID = primitive.NewObjectID()
}

// DateFields - `created_at`, `updated_at` and `deleted_at`
type DateFields struct {
	CreatedAt time.Time `bson:"created_at"`
	UpdatedAt time.Time `bson:"updated_at"`
	DeletedAt time.Time `bson:"deleted_at"`
}

// Init - Create action for date field
func (m *DateFields) Init() {
	m.CreatedAt = time.Now().UTC()
}

// Update - Update action for date field
func (m *DateFields) Update() {
	m.UpdatedAt = time.Now().UTC()
}

// Delete - Delete action for date field
func (m *DateFields) Delete() {
	m.DeletedAt = time.Now().UTC()
}

// BaseModel - Default model
type BaseModel struct {
	IDField    `bson:",inline"`
	DateFields `bson:",inline"`
}

// Init - Initialize Model
func (m *BaseModel) Init() {
	m.IDField.Init()
	m.DateFields.Init()
}

// Update - Initialize Model
func (m *BaseModel) Update() {
	m.DateFields.Update()
}

// Delete - Initialize Model
func (m *BaseModel) Delete() {
	m.DateFields.Delete()
}

// GetID - return id
func (m *BaseModel) GetID() primitive.ObjectID {
	return m.ID
}

// GetCreatedAt - return id
func (m *BaseModel) GetCreatedAt() time.Time {
	return m.CreatedAt
}

// GetUpdatedAt - return id
func (m *BaseModel) GetUpdatedAt() time.Time {
	return m.UpdatedAt
}

// GetDeletedAt - return id
func (m *BaseModel) GetDeletedAt() time.Time {
	return m.DeletedAt
}
