package models

import (
	"testing"
)

func TestInitializeUserModel(t *testing.T) {
	expected := map[string]interface{}{
		"username": "uname1234",
		"password": "hashed1234",
		"fullname": "John Doe",
		"email":    "my@mailinator.com",
		"mobile":   "+62 876 9876 5432",
		"enabled":  true,
	}
	user := &User{
		Username:     expected["username"].(string),
		Fullname:     expected["fullname"].(string),
		Email:        expected["email"].(string),
		MobileNumber: expected["mobile"].(string),
		Enabled:      expected["enabled"].(bool),
	}

	assertModel(t, user)
}
