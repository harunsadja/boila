package repositories

import (
	"context"

	"github.com/harunrasyid/boila/app/gateways"
	"github.com/harunrasyid/boila/app/models"
	"github.com/harunrasyid/boila/app/schemas"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// User - User repo struct
type User struct {
	Gateway     gateways.GatewayInterface
	Transformer *schemas.UserTransformer
}

// FindByID - Self defined
func (u *User) FindByID(ctx context.Context, ID string) (
	*schemas.User,
	*schemas.Meta,
) {
	id, err := primitive.ObjectIDFromHex(ID)
	if err != nil {
		return notFound()
	}
	r, err := u.Gateway.FindByID(ctx, id)
	user := r.(*models.User)
	if err != nil {
		return nil, schemas.InternalServerErrorMeta()
	}
	if user.ID == primitive.NilObjectID {
		return notFound()
	}

	meta := &schemas.Meta{
		Status:  200,
		Message: "Ok",
	}
	return u.Transformer.Item(user, "brief"), meta
}

func notFound() (*schemas.User, *schemas.Meta) {
	return nil, &schemas.Meta{
		Status:  404,
		Message: "Not Found",
	}
}
