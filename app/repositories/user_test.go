package repositories

import (
	"context"
	"reflect"
	"testing"

	"github.com/harunrasyid/boila/app/models"
	"github.com/harunrasyid/boila/app/schemas"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func defineSingleModel(data map[string]interface{}) *models.User {
	var model *models.User = &models.User{
		Username:     data["username"].(string),
		Fullname:     data["fullname"].(string),
		Email:        data["email"].(string),
		MobileNumber: data["mobile_number"].(string),
		Enabled:      data["enabled"].(bool),
	}
	model.Init()
	return model
}

func getExpected() []*models.User {
	var data []*models.User
	data = append(data, defineSingleModel(map[string]interface{}{
		"username":      "myusername1",
		"fullname":      "Harun Al Rasyid",
		"email":         "my@email.domain1",
		"mobile_number": "085287654321",
		"enabled":       true,
	}))
	data = append(data, defineSingleModel(map[string]interface{}{
		"username":      "myusername2",
		"fullname":      "Harun Al Rasyid",
		"email":         "my@email.domain2",
		"mobile_number": "085287654322",
		"enabled":       true,
	}))
	data = append(data, defineSingleModel(map[string]interface{}{
		"username":      "myusername3",
		"fullname":      "Harun Al Rasyid",
		"email":         "my@email.domain3",
		"mobile_number": "085287654323",
		"enabled":       true,
	}))
	return data
}

var expected []*models.User = getExpected()

type gatewayMock struct {
}

func (g *gatewayMock) FindByID(ctx context.Context, ID primitive.ObjectID) (interface{}, error) {
	return expected[0], nil
}

func TestUser(t *testing.T) {
	wantedUser := &schemas.User{
		ID:       expected[0].ID.Hex(),
		Username: expected[0].Username,
		Fullname: expected[0].Fullname,
		Enabled:  expected[0].Enabled,
	}

	wantedMeta := &schemas.Meta{
		Status:     200,
		Message:    "Ok",
		Pagination: nil,
	}

	u := User{
		Gateway:     &gatewayMock{},
		Transformer: &schemas.UserTransformer{},
	}

	user, meta := u.FindByID(
		context.Background(),
		expected[0].ID.Hex(),
	)

	if !reflect.DeepEqual(wantedUser, user) {
		t.Errorf("Want not same with got")
	}

	if !reflect.DeepEqual(wantedMeta, meta) {
		t.Errorf("Want not same with got")
	}
}
