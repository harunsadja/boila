package abstractions

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
)

// SingleResultInterface - Self Defined
type SingleResultInterface interface {
	Decode(interface{}) error
}

// CollectionInterface - Self Defined
type CollectionInterface interface {
	FindOne(context.Context, interface{}) SingleResultInterface
}

// DBInterface - Self Defined
type DBInterface interface {
	Collection(string) CollectionInterface
}

// MongoSingleResult - Wrapper of mongo single result
type MongoSingleResult struct {
	Sr *mongo.SingleResult
}

// Decode - Run single result decode
func (sr *MongoSingleResult) Decode(v interface{}) error {
	return sr.Sr.Decode(v)
}

// MongoCollection - Wrapper of mongo collection
type MongoCollection struct {
	C *mongo.Collection
}

// FindOne - Find one record
func (c *MongoCollection) FindOne(ctx context.Context, filter interface{}) SingleResultInterface {
	sr := c.C.FindOne(ctx, filter)
	return &MongoSingleResult{
		Sr: sr,
	}
}

// MongoDB - DB mongo abstraction
type MongoDB struct {
	DB *mongo.Database
}

// Collection - DB collection definition
func (db *MongoDB) Collection(name string) CollectionInterface {
	collection := db.DB.Collection(name)
	return &MongoCollection{
		C: collection,
	}
}
