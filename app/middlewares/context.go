package middlewares

import (
	"context"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/harunrasyid/boila/utils"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

// ContextMiddleware - Configentication middleware
type ContextMiddleware struct {
	Config *viper.Viper
}

// Middleware - Context middleware function
func (c *ContextMiddleware) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Add timeout to context
		ctx, cancel := context.WithTimeout(r.Context(), time.Second*2)
		defer cancel()

		// Add config to context
		var configKey utils.ContextKey = "config"
		ctx = context.WithValue(ctx, configKey, c.Config)

		// Add logger to context
		const logfile = "./logs/all.log"
		outfile, err := os.OpenFile(logfile, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0755)
		if err != nil {
			log.Fatal("Error open log file")
		}
		env := c.Config.GetString("app_env")
		logger := logrus.New()
		logger.SetReportCaller(true)
		logger.SetFormatter(&logrus.JSONFormatter{})
		if env == "dev" {
			logger.SetLevel(logrus.WarnLevel)
		} else {
			logger.SetLevel(logrus.DebugLevel)
			logger.SetOutput(outfile)
		}
		logger.ExitFunc = func(x int) {
			if outfile != nil {
				outfile.Close()
			}
		}

		var loggerKey utils.ContextKey = "logger"
		ctx = context.WithValue(ctx, loggerKey, logger)

		r = r.WithContext(ctx)
		next.ServeHTTP(w, r)
	})
}
