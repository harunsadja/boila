package gateways

import (
	"context"
	"testing"

	"github.com/harunrasyid/boila/app/abstractions"
	"github.com/harunrasyid/boila/app/models"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func getExpected() map[string]interface{} {
	ID, _ := primitive.ObjectIDFromHex("5ea919531880ed15087e18ad")

	// Model definition without initiation.
	var md models.User = models.User{
		Username:     "myusername",
		Fullname:     "Harun Al Rasyid",
		Email:        "my@email.domain",
		MobileNumber: "085287654321",
		Enabled:      true,
	}

	return map[string]interface{}{
		"id":     ID,
		"result": md,
	}
}

type singleResultMock struct {
}

func (s *singleResultMock) Decode(v interface{}) error {
	expected := getExpected()
	*v.(*models.User) = expected["result"].(models.User)
	return nil
}

type collectionMock struct {
}

func (c *collectionMock) FindOne(
	ctx context.Context,
	filter interface{},
) abstractions.SingleResultInterface {
	return &singleResultMock{}
}

type dbMock struct {
}

func (d *dbMock) Collection(name string) abstractions.CollectionInterface {
	return &collectionMock{}
}

func TestFindById(t *testing.T) {
	expected := getExpected()
	t.Run("Test get single document by it's id", func(t *testing.T) {
		want := expected["result"].(models.User)
		user := User{
			DB: &dbMock{},
		}

		r, _ := user.FindByID(context.Background(), expected["id"].(primitive.ObjectID))
		got := r.(*models.User)

		if *got != want {
			t.Errorf("Want not same with got")
		}
	})
}
