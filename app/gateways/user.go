package gateways

import (
	"context"

	"github.com/harunrasyid/boila/app/abstractions"
	"github.com/harunrasyid/boila/app/models"
	"github.com/harunrasyid/boila/utils"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// User - User gateway
type User struct {
	DB abstractions.DBInterface
}

// FindByID - Find a single user by it's ID
func (u *User) FindByID(ctx context.Context, ID primitive.ObjectID) (interface{}, error) {
	collection := defineCollection(u.DB)
	user := models.User{}
	sr := collection.FindOne(ctx, bson.M{"_id": ID})
	err := sr.Decode(&user)
	if err != nil {
		if err.Error() == "mongodb: no rows in result set" {
			err = nil
		} else {
			ctx.Value(utils.ContextKey("logger")).(*logrus.Logger).Fatal(err)
		}
	}

	return &user, err
}

func defineCollection(db abstractions.DBInterface) abstractions.CollectionInterface {
	return db.Collection("user")
}
