package gateways

import (
	"context"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// GatewayInterface - self defined
type GatewayInterface interface {
	FindByID(context.Context, primitive.ObjectID) (interface{}, error)
}
