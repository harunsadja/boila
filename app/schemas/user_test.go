package schemas

import (
	"reflect"
	"testing"

	"github.com/harunrasyid/boila/app/models"
)

func defineSingleModel(data map[string]interface{}) *models.User {
	var model *models.User = &models.User{
		Username:     data["username"].(string),
		Fullname:     data["fullname"].(string),
		Email:        data["email"].(string),
		MobileNumber: data["mobile_number"].(string),
		Enabled:      data["enabled"].(bool),
	}
	model.Init()
	return model
}

func getExpected() []*models.User {
	var data []*models.User
	data = append(data, defineSingleModel(map[string]interface{}{
		"username":      "myusername1",
		"fullname":      "Harun Al Rasyid",
		"email":         "my@email.domain1",
		"mobile_number": "085287654321",
		"enabled":       true,
	}))
	data = append(data, defineSingleModel(map[string]interface{}{
		"username":      "myusername2",
		"fullname":      "Harun Al Rasyid",
		"email":         "my@email.domain2",
		"mobile_number": "085287654322",
		"enabled":       true,
	}))
	data = append(data, defineSingleModel(map[string]interface{}{
		"username":      "myusername3",
		"fullname":      "Harun Al Rasyid",
		"email":         "my@email.domain3",
		"mobile_number": "085287654323",
		"enabled":       true,
	}))
	return data
}

func TestTransformItem(t *testing.T) {
	expected := getExpected()

	t.Run("Test transform single item", func(t *testing.T) {
		want := &User{
			ID:       expected[0].ID.Hex(),
			Username: expected[0].Username,
			Fullname: expected[0].Fullname,
		}

		transformer := UserTransformer{}
		got := transformer.Item(expected[0], "reference")

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Want not same with got")
		}
	})
}

func TestTransformCollection(t *testing.T) {
	expected := getExpected()

	t.Run("Test transform collection", func(t *testing.T) {
		transformer := UserTransformer{}
		got := transformer.Collection(expected, "brief")
		if !assertCollection(got, expected) {
			t.Errorf("Want not same with got")
		}
	})
}

func assertCollection(got []*User, expected []*models.User) bool {
	for i, data := range got {
		if data.ID != expected[i].ID.Hex() {
			return false
		}
		if data.Username != expected[i].Username {
			return false
		}
		if data.Fullname != expected[i].Fullname {
			return false
		}
		if data.Enabled != expected[i].Enabled {
			return false
		}
	}
	return true
}
