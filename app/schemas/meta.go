package schemas

import (
	"math"
)

// Pagination meta struct
type Pagination struct {
	Total       int `json:"total_data"`
	PerPage     int `json:"records_per_page"`
	CurrentPage int `json:"current_page"`
	MaxPage     int `json:"max_page"`
}

// Meta - Meta model struct
type Meta struct {
	Status     int         `json:"status"`
	Message    string      `json:"message"`
	Pagination *Pagination `json:"pagination,omitempty"`
}

// GeneratePaginationMeta - Self Defined
func GeneratePaginationMeta(total int, perPage int, currentPage int) *Pagination {
	return &Pagination{
		Total:       total,
		PerPage:     perPage,
		CurrentPage: currentPage,
		MaxPage:     int(math.Ceil(float64(total / perPage))),
	}
}

// InternalServerErrorMeta - Self Defined
func InternalServerErrorMeta() *Meta {
	return &Meta{
		Status:  500,
		Message: "Internal Server Error",
	}
}

// BadRequestMeta - Self Defined
func BadRequestMeta(err error) *Meta {
	return &Meta{
		Status:  400,
		Message: "Bad Request",
	}
}
