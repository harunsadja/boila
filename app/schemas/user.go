package schemas

import (
	"time"

	"github.com/harunrasyid/boila/app/models"
)

// User - data model of user
type User struct {
	ID           string    `json:"_id,omitempty"`
	Username     string    `json:"username,omitempty"`
	Password     string    `json:"password,omitempty"`
	Fullname     string    `json:"fullname,omitempty"`
	Email        string    `json:"email,omitempty"`
	MobileNumber string    `json:"mobile_number,omitempty"`
	Enabled      bool      `json:"enabled,omitempty"`
	CreatedAt    time.Time `json:"created_at,omitempty"`
	UpdatedAt    time.Time `json:"updated_at,omitempty"`
}

// UserTransformer - transformer for repeat user
type UserTransformer struct{}

// Item - Transform single data
func (t *UserTransformer) Item(data *models.User, verbosity string) *User {
	switch verbosity {
	case "reference":
		return &User{
			ID:       data.ID.Hex(),
			Username: data.Username,
			Fullname: data.Fullname,
		}
	case "brief":
		return &User{
			ID:       data.ID.Hex(),
			Username: data.Username,
			Fullname: data.Fullname,
			Enabled:  data.Enabled,
		}
	default:
		return &User{
			ID:           data.ID.Hex(),
			Username:     data.Username,
			Fullname:     data.Fullname,
			Email:        data.Email,
			MobileNumber: data.MobileNumber,
			Enabled:      data.Enabled,
			CreatedAt:    data.CreatedAt,
			UpdatedAt:    data.UpdatedAt,
		}
	}
}

// Collection - Transform data collection
func (t *UserTransformer) Collection(data []*models.User, verbosity string) []*User {
	var result []*User
	for _, i := range data {
		result = append(result, t.Item(i, verbosity))
	}

	return result
}
